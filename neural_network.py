from multiprocessing.sharedctypes import Value
import numpy as np
import matplotlib.pyplot as plt
from numpy.core.function_base import linspace
import pandas as pd
import math

def sigmoid(value):
    return 1/(1-np.exp(value))

def neur_net(x,t,w):
    """[summary]

    Args:
        x (np.array): input data
        t (np.array): target
        w (np.array): weight

    Returns:
        np.array: gradW 
        np.array: loss
    """
    # FORWARD PROPAGATION
    h1_in = x[0]*w[0] + x[1]*w[2]
    h2_in = x[0]*w[2] + x[1]*w[3]
    
    h1_out = 1/(1+math.exp(-h1_in))
    h2_out = 1/(1+math.exp(-h2_in))
    
    y1_in = w[4]*h1_out + w[6]*h2_out
    y2_in = w[5]*h1_out + w[7]*h2_out
    
    y1_out = 1/(1+np.exp(-y1_in))
    y2_out = 1/(1+np.exp(-y2_in))    
    l = pow((y1_out - t[0]),2) + pow((y2_out - t[1]),2)
    
    # BACKWARD PROPAGATION
    gradW = np.zeros(np.shape(w))
    
    dl_y1_out = 2*(y1_out - t[0])
    dl_y2_out = 2*(y2_out - t[1])
    
    dl_y1_in = dl_y1_out * (y1_out * (1-y1_out))
    dl_y2_in = dl_y2_out * (y2_out * (1-y2_out))
    
    gradW[7] = np.around(dl_y2_in * h2_out,3)
    gradW[6] = np.around(dl_y1_in * h2_out,3)
    gradW[5] = np.around(dl_y2_in * h1_out,3)
    gradW[4] = np.around(dl_y1_in * h1_out,3)
    
    dl_h1_out = dl_y1_in*w[4] + dl_y2_in*w[5]
    dl_h2_out = dl_y1_in*w[6] + dl_y2_in*w[7]
    
    dl_h1_in = dl_h1_out*(h1_out*(1-h1_out))
    dl_h2_in = dl_h2_out*(h2_out*(1-h2_out))

    gradW[3] = np.around(dl_h2_in * x[1],3)
    gradW[2] = np.around(dl_h1_in * x[1],3)
    gradW[1] = np.around(dl_h2_in * x[0],3)
    gradW[0] = np.around(dl_h1_in * x[0],3)
    
    return gradW ,l

def training(data,target,weigth,Niter=100,step=1):
    list_loss = []
    i = 1
    delL = 0
    
    while(i<=Niter):
        gradW,l =neur_net(data,target,weigth)
        delL = delL + gradW/i
        weigth = weigth - step*delL
        list_loss.append(l)
        i +=1
    pass

    return list_loss

def train_test(data,target,weigth,batch,epochs,step):
    length = np.shape(data)
    new_weigth = weigth
    nbr_batch = int(length[0]/batch)
    list_lost = []
    grad_sum = 0
    value = 0
    
    for i_epoch in range (0,epochs):
        for i_nbr_batch in range (0,nbr_batch):
            for i_batch in range (0,batch):
                x = np.transpose(data[i_batch+value,:])
                targ = np.transpose(target[i_batch+value,:])
                
                gradW,loss = neur_net(x,targ,new_weigth)
                grad_sum = grad_sum + gradW
            pass
            grad_mean = grad_sum/batch
            new_weigth = new_weigth - step*grad_mean
            grad_sum = 0
            grad_mean = 0
            if batch != 200:
                value = value + batch
        pass
        value = 0
        list_lost.append(loss)
    pass
    return list_lost


def main():
    #### DATA
    x1 = np.array([[2],[1]]) #data
    t1 = np.array([[1],[0]]) #desired target

    x2 = np.array([[-1],[3]])
    t2 = np.array([[0],[1]])
    
    x3 = np.array([[1],[4]])
    t3 = np.array([[1],[0]])

    data = pd.read_csv("TraningData.data")
    x4 = data.iloc[:,0]
    x5 = data.iloc[:,1]
    t4 = data.iloc[:,2]
    t5 = data.iloc[:,3]
    
    data = pd.read_csv("Test.data")
    x6 = data.iloc[:,0]
    x7 = data.iloc[:,1]
    t6 = data.iloc[:,2]
    t7 = data.iloc[:,3]
    
    w = np.array([2,-3,-3,4,1,-1,0.25,2]) #weight
    w = np.transpose(w)
    
    #### NETWORK
    gradW1,l1 = neur_net(x1,t1,w)
    gradW2,l2 = neur_net(x2,t2,w)
    
    print("Gradient 1:\n",gradW1)
    print("Loss 1:\n",l1)
    
    print("Gradient 2:\n",gradW2)
    print("Loss 2:\n",l2)
    
    #### TRAINING
    step_1 = training(x3,t3,w,100,1)
    step_10 = training(x3,t3,w,100,10)
    step_01 = training(x3,t3,w,100,0.1)
    Niter = linspace(0,100,100)
    plt.plot(Niter,step_1,linewidth=4)
    plt.plot(Niter,step_10,linewidth=4)
    plt.plot(Niter,step_01,linewidth=4)
    plt.xlabel("Number of Iteration",fontsize=16)
    plt.xticks(fontsize = 15)
    plt.yticks(fontsize = 15)
    plt.ylabel("Loss",fontsize=16)
    plt.title("Evolution of the loss with the third target and value",fontsize=16)
    plt.legend(['Step = 1','Step = 10','Step = 0.1'],fontsize=16)
    plt.show()
    
    #### TRAINING & TESTING
    x_tt = np.transpose(np.array([x4,x5]))
    t_tt = np.transpose(np.array([t4,t5]))
    batch = 200
    epoch = 8
    step = 10
    
    list_tt =train_test(x_tt,t_tt,w,batch,epoch,step)
    list_epoch_8_batch_100 =train_test(x_tt,t_tt,w,100,epoch,step)
    list_epoch_8_batch_10 =train_test(x_tt,t_tt,w,10,epoch,step)
    
    Ntest = linspace(0,epoch,epoch)
    
    plt.plot(Ntest,list_tt)
    plt.plot(Ntest,list_epoch_8_batch_100)
    plt.plot(Ntest,list_epoch_8_batch_10)
    
    
    plt.xlabel("Number of Epoch",fontsize=16)
    plt.xticks(fontsize = 15)
    plt.yticks(fontsize = 15)
    plt.ylabel("Loss",fontsize=16)
    plt.title("Evolution of the Loss with Dataset Step =10",fontsize=16)
    plt.legend(['Batch = 200','Batch = 100','Batch = 10'],fontsize=16)

    plt.show()
    
    #### LAST DATASET
    x_ld = np.transpose(np.array([x6,x7]))
    t_ld = np.transpose(np.array([t6,t7]))
    list_ld =[]
    list_train =[]
    N_ld = linspace(0,100,100)
    N_ldplus = linspace(0,100,100)
    
    for i in range(0,100):
        gradW,loss_ld = neur_net(x_ld[i,:],t_ld[i,:],w)
        loss_train = training(x_ld[i,:],t_ld[i,:],w,Niter=100,step=5)
        # loss_train = train_test(x_ld,t_ld,w,1,2,10)
        list_train.append(loss_train[-1])
        list_ld.append(loss_ld)
        pass
    
    
    plt.figure()
    plt.plot(N_ld,list_ld,linewidth=4)
    plt.xlabel("Elements in Data",fontsize=16)
    plt.xticks(fontsize = 15)
    plt.yticks(fontsize = 15)
    plt.ylabel("Loss",fontsize=16)
    plt.title("Loss obtained to each elements in network Before Train",fontsize=16)
    plt.show()
    
    plt.figure()
    plt.plot(N_ldplus,list_train,linewidth=4)
    plt.xlabel("Elements in Data",fontsize=16)
    plt.xticks(fontsize = 15)
    plt.yticks(fontsize = 15)
    plt.ylabel("Loss",fontsize=16)
    plt.title("Loss obtained to each elements in network After Train",fontsize=16)
    plt.show()
    
    

    
    
    
    pass

if __name__ == "__main__":
    main()